import { Component, OnInit } from '@angular/core';
import { ApiService } from '../service/api.service'
@Component({
  selector: 'app-questions-answerds',
  templateUrl: './questions-answerds.component.html',
  styleUrls: ['./questions-answerds.component.css']
})
export class QuestionsAnswerdsComponent implements OnInit {
  panelOpenState = false;
  qa = []
  updatedTime = ''
  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.apiService.fetchData().subscribe((data: any) =>{
      this.qa = data.qa
      this.updatedTime = data.updatedTime
    })
  }

}
