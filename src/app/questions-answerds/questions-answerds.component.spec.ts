import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionsAnswerdsComponent } from './questions-answerds.component';

describe('QuestionsAnswerdsComponent', () => {
  let component: QuestionsAnswerdsComponent;
  let fixture: ComponentFixture<QuestionsAnswerdsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuestionsAnswerdsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionsAnswerdsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
